**Previous Page:** [Getting Started With Go](./01a_getting_started_with_go.md)  
**Next Page:** [Refactoring, Testing, and Linting](./01c_refactoring_testing_and_linting.md)  

# Writing a Web App With Go

## Obective

To create a simple web application capable of responding to different URI
patterns to perform different actions.


## Overview

The Go language along with the third party
[Gin HTTP framework](https://gin-gonic.github.io/gin/) will be used to develop
the application.

To keep things simple, we'll implement the following two routes:

* `GET /`: Reply with the web app's name and version info
* `GET /greet/NAME`: Reply with a greeting directed to the given name


## First Pass

Here's a quick first pass that doesn't lend itself well to testing or
maintainability:

```go
# main.go
package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
  router := gin.Default()
	router.GET("/", func(c *gin.Context) {
    c.String(200, "luka 0.0.1")
  })
	router.GET("/greet/:name", func(c *gin.Context) {
    c.String(200, "Hello, " + c.Param("name"))
  })
  router.Run()
}
```

The `gin` library is imported and then used to spawn a Gin router with a
default configuration. We then define the two route paths with HTTP
response codes and custom functions to perform for each and then run the
router in a loop that will listen for web traffic.


## Running

First, the dependency on the `gin-gonic/gin` package must be satisfied. Use
`go get` to fetch the package:
```shell
go get github.com/gin-gonic/gin
```

To run the app, save the source code as `main.go` and then use:
```shell
go run main.go
```

As the web server begins listening for traffic you may be prompted to allow it
to do so.

For quick debugging and development, Go's built-in `go run` functionality works
well. For web applications in particular, a code reloading solution may be used
to avoid having to stop the `go run` process and restart it every time a code
change is made.

It is also possible to compile the application into an operating system
specific single executable containing all of the necessary libraries.

To compile the `main.go` source and run it, do:
```shell
go build -o luka main.go
./luka
```

The `-o luka` option specifies that the output binary will be named `luka`.
By default, the name of the source code file (`main.go`) would be used
(`main`).


## Integration Testing

With the `luka` app running, either via `go run` or as an executable, use a
separate terminal window to test the web app with the `curl` utility. The
Gin router will listen on port `8080` by default and we can test the two
defined routes:
```shell
% curl http://localhost:8080
luka 0.0.1%

% curl http://localhost:8080/greet/Luka
Hello, Luka%
```

Your shell may or may not produce newlines on the end of the curl responses
that are printed out. If you do not have access to curl or would prefer to use
a web browser, paste the two URLs one at a time from the curl example above
into a web browser's address bar and hit Enter/Return.


## Using Make

The [Make](https://en.wikipedia.org/wiki/Make_(software)) tool is a tried and
true programmer's assistant dating back to 1976. By defining a `Makefile`
for a project, a programmer can have Make automate various processes such
as compiling, running, prepping, and cleaning up.

Let's take everything we've done up to this point and make it repeatable
and automated by creating a `Makefile` file alongside the `main.go` file
in the same directory. NOTE that the `Makefile` uses tabs for indentation.
```makefile
# Makefile
build: deps
	go build -o luka main.go
clean:
	rm -f luka
curl:
	curl http://localhost:8080
	curl http://localhost:8080/greet/Luka
deps:
	go get github.com/gin-gonic/gin
run: deps
	go run main.go
```

The `deps` target, which satisfies the project's dependencies will
automatically be invoked first when the `build` or `run` targets are
invoked.

The targets have been listed in the `Makefile` file in alphabetical order but
this is not a requirement.

With the `Makefile` file in place, the following commands can be used to
accomplish things:

* `make build`: Use `go build` to compile the source code into an executable
* `make clean`: Remove the binary produced by `make build`
* `make curl`: With the web app running in a separate process, launch `curl` tests
             against it
* `make deps`: Use `go get` to satisfy the project dependencies
* `make run`: Use `go run` to run the web application


**Previous Page:** [Getting Started With Go](./01a_getting_started_with_go.md)  
**Next Page:** [Refactoring, Testing, and Linting](./01c_refactoring_testing_and_linting.md)  
