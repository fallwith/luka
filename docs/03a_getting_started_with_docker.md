**Previous Page:** [Triggering Tests and Linting With GitLab](./02b_triggering_tests_and_linting_with_gitlab.md)  
**Next Page:** [Incorporating Containers In the Development Process](./03b_incorporating_containers_in_the_development_process.md)  

# Getting Started With Docker

## Containers

The term "containers" in this context refers to leveraging the virtualization
of an operating system in order to neatly pack a process and its dependencies
together in a single package that has access only to the resources its contents
requires.

One would use containers as opposed to setting up a host machine directly. For
both local development and deployments, this approach provides a good number of
benefits:

* **Consistency** - The contents packed into a container do not change until the
                container is rebuilt. Setting up a machine directly may
                require internet access, may involve different versions of
                software being installed depending on the installation date,
                and may result in errors or differences each time setup is
                performed. With containers, a consistent experience is assured.
* **Efficiency** - Containers can be restricted to not consume too many system
               resources, such as CPU, memory, or disk usage. Beyond the
               obvious benefit of preventing a rogue process from taking up
               too many resources, the container approach in some cases may
               also allow for more processes to be packed into the same
               hardware than would be if the hardware was provisioned directly.
* **Versioning** - Once a container is built and known to be in good working order,
               its version can be recorded as something to fall back to if a
               future container build has issues. This allows for issue
               tracking and confidence in trying new configurations.
* **Tagging** - Each container can be associated with one or more tags that serve
            to label the container as to its purpose and capabilities. This
            allows the team responsible for maintaining deployed containers to
            quickly identify and take action against containers.
* **Security** - When following certain community standard best practices of
             container architecting such as focusing only on a single process
             and using a read only file system, the use of containers can help
             with securing an infrastructure.
* **Speed** - A container image contains a frozen snapshot of state. After a
          container is launched from a container image, that container's
          processes and resources are already running and configured. This
          provides for a great speed advantage over systems requiring a full
          boot sequence or provisioning process after launch.


## Docker

Docker is software produced by the company of the same name to generate and
run containers. Docker was released as open source software to the public in
2013. It's popularity and open source nature make it an ideal choice for
learning about containers.


### Setup

Head to the [Docker Community Edition](https://www.docker.com/community-edition)
web page and follow the instructions there in order to install Docker on your
machine.

For macOS users who would prefer to use [Homebrew](https://brew.sh), perform
`brew cask install docker` to install Docker.

Once Docker is installed and working properly, proceed to the next section.


**Previous Page:** [Triggering Tests and Linting With GitLab](./02b_triggering_tests_and_linting_with_gitlab.md)  
**Next Page:** [Incorporating Containers In the Development Process](./03b_incorporating_containers_in_the_development_process.md)  
