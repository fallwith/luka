**Previous Page:** [Refactoring, Testing, and Linting](./01c_refactoring_testing_and_linting.md)  
**Next Page:** [Triggering Tests and Linting With GitLab](./02b_triggering_tests_and_linting_with_gitlab.md)  

# Getting Started With GitLab

## Continuous Integration

Continuous Integration (CI) is a software development practice of continually
integrating changes - typically coming in from multiple authors - into a
single project.

CI typically involves the following as gates that changes must pass through:

* manual code review
* automated code review (linting and style guide adherence)
* automated building and/or testing

These gates are important to protect things such as the stability,
maintainability, and performance of the project.

This guide will GitLab with a focus on the two automated parts of the CI
process.


## GitLab

[GitLab](https://gitlab.com) is an open source web based
[Git](https://git-scm.com) repository manager aiming to be a one stop shop
for handling all stages of the software development and deployment lifecycle.
GitLab offers analytics, code review tools, issue tracking, automated
continuous integration, release, and deployment tools, container functionality, 
metrics and monitoring, and security and policy related scanning tools.

GitLab offers software as a service (SaaS) hosted cloud solutions at
GitLab.com, and self-hosted on-premises solutions.

GitLab's open sourced, modern, and feature rich approach is accessible via
free tiers - making for an ideal introduction to continuous integration and
delivery (CI/CD) that will be valuable and relevant outside of the tutorial.

For this guide, we'll be using the publicly hosted cloud offerings available at
GitLab.com.

Please complete the following prerequisite steps before proceeding:

### Steps

* Go to the [GitLab.com sign in page](https://gitlab.com/users/sign_in) and
  register for an account
* Once registered and signed in, visit your
  [Profile](https://gitlab.com/profile) and make any desired changes
* Visit the [SSH Keys](https://gitlab.com/profile/keys) section of your profile
  and upload a public SSH key for use with interacting with GitLab. If you do
  not already have any SSH keys or any suitable for this purpose, follow the
  instructions provided on the page for generating a new one.

Once you've created and populated your profile and associated a public SSH key
with it, proceed to the next page.


**Previous Page:** [Refactoring, Testing, and Linting](./01c_refactoring_testing_and_linting.md)  
**Next Page:** [Triggering Tests and Linting With GitLab](./02b_triggering_tests_and_linting_with_gitlab.md)  
