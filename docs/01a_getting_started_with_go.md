**Previous Page:** [README](./../README.md)  
**Next Page:** [Writing a Web App With Go](./01b_writing_a_web_app_with_go.md)  

# Getting Started With Go

Go (also known as Golang) is an open source programming language created at
Google in 2009 by Robert Griesemer, Rob Pike, and Ken Thompson. The
[Go homepage](https://golang.org) provides an interactive introduction
to the language.


## Setup

Read through the Go homepage's
[installation instructions](https://golang.org/doc/install) to install Go and
test that it's working.

For macOS users who would prefer to use
[Homebrew](https://brew.sh/), perform `brew install go` to install Go.


CHANGE HERE.  either default or custom location, but never set GOROOT


Somewhere in your shell's init files, add your Go workspace's `bin` directory
to your system `PATH`:
```
# for the default workspace path, $HOME/go
export PATH=$HOME/go/bin:$PATH

# for a custom workspace path, set GOPATH as well
export GOPATH=/path/to/workspace
mkdir -p $GOPATH/bin
export PATH=$GOPATH/bin:$PATH
```


## Confirmation

Follow the Go homepage's
[Test your installation](https://golang.org/doc/install#testing)
instructions to verify that you can build and run the "hello, world"
example.

With a shell session that has both `go` itself and the Go workspace's `bin` dir
set in `$PATH`, test the downloading of the
[Go Meta Linter](https://github.com/alecthomas/gometalinter) tool with `go get`
and then confirm that the tool can be executed. If that works, proceed to
have the Go Meta Linter install all of the linters it supports.
```
# Confirm that go is in the PATH
go version

# Download Go Meta Linter and make sure its executable is in the PATH
go get github.com/alecthomas/gometalinter
gometalinter --version

# Install Go Meta Linter's supported linters
gometalinter --install
```


## (Optional) Resources for learning Go

* [A Tour of Go](https://tour.golang.org/list)
* [Let's learn Go!](http://go-book.readthedocs.io/en/latest/)
* [A Gopher's Reading List](https://github.com/enocom/gopher-reading-list)
* [How to Write Go Code](https://golang.org/doc/code.html)
* [Go by Example](https://gobyexample.com/)
* [Go Koans](https://github.com/cdarwin/go-koans)


Once done, head to the next page.


**Previous Page:** [README](./../README.md)  
**Next Page:** [Writing a Web App With Go](./01b_writing_a_web_app_with_go.md)  
