**Previous Page:** [The Kubernetes Dashboard](./04c_the_kubernetes_dashboard.md)  
**Next Page:** [Getting Started With Google Cloud Platform (GCP)](./05a_getting_started_with_google_cloud_platform_gcp.md)  

# Stopping and Removing Kubernetes Resources

## Overview

Minikube's running virtual machine and the Kubernetes resources it serves will
continue to hold on to the system resources allocated to them until stopped.

Once you are done interacting with your local Kubernetes cluster, follow these
instructions to stop Minikube and to delete all of the Luka project related
content if desired.


## Stopping Minikube

The entirety of our local Kubernetes cluster lives within Minikube's running
virtual machine. Simply shutting it down will free up memory and CPU resources,
while preserving all existing content on disk.

```shell
minikube stop
```

With the virtual machine stopped, the `kubectl` tool will no longer work, as it
won't be able to locate a running Kubernetes cluster.


## Deleting the Minikube based Kubernetes cluster

Minikube can be started back up again with `minikube start`. If you'd rather go
ahead and discard the Kubernetes cluster, perform `minikube delete`.

```shell
# this deletes the Kubernetes cluster, not Minikube itself
minikube delete
```


## Deleting Minikube

Once the Minikube virtual machine has been stopped and the Kubernetes cluster
deleted, a fresh cluster can be created again with `minikube create`. If you'd
like to delete Minikube itself, simply remove the `.minikube` and `.kube`
directories beneath your user account's home directory.

```
rm -rf ~/.minikube
rm -rf ~/.kube
```


## Deleting individual Kubernetes resources

Shy of deleting the entire Kubernetes cluster or Minikube itself, you may also
wish to delete individual Kubernetes resources from the cluster or the
Docker images used to power pods.

Note that there is no need to follow these steps if you've already deleted
Minikube.

The `kubectl` tool supports a `delete` command for deleting resources.
Deletions can be scoped to a specific resource type or a given namespace, or
left unchecked with the `--all` switch like so:

```shell
kubectl delete pods,services,clusterrolebindings,jobs,secrets,serviceaccounts,replicasets --all
```

For more information on what `kubectl` can do, consult the
[kubectl Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/).

As explained previously, Minikube uses its own local Docker registry that is
separate from your system's default registry. To delete a Docker container or
image associated with Minikube, run the `export` commands yielded by
`minikube docker-env` just as was done to add images to the Minikube
registry.

```shell
eval $(minikube docker-env)
docker rm <container id>
docker rmi <image id>
```

**Previous Page:** [The Kubernetes Dashboard](./04c_the_kubernetes_dashboard.md)  
**Next Page:** [Getting Started With Google Cloud Platform (GCP)](./05a_getting_started_with_google_cloud_platform_gcp.md)  
