**Previous Page:** [Incorporating Containers In the Development Process](./03b_incorporating_containers_in_the_development_process.md)  
**Next Page:** [Getting Started With Kubernetes](./04a_getting_started_with_kubernetes.md)  

# Incorporating Containers In the CI/CD Process

## Overview

Containers can be incorporated into the continuous integration / continuous
deployment (CI/CD) process in a number of ways, including the automation of
complex integration tests. For now, we'll focus on building and pushing
containers to make them available for use.

Previously we leveraged GitLab's CI/CD pipeline system to automate the running
of linters and unit tests against our Go application code. Now we'll take that
setup a step further to compile the Go code, build a Docker image, and push
the Docker image to a remote repository.

GitLab offers its own tools for working with containers that we'll be
making use of. These tools are all open source and standards based, so a
familiarity with them should prepare you well for working with containers
elsewhere as well.

GitLab came out with their own container registry in 2016 and explained it very
well in a [blog post](https://about.gitlab.com/2016/05/23/gitlab-container-registry/).
A container registry allows for the networked, centralized storage and tagging
of container images. This allows multiple developers or even multiple teams to
share secure access to the same set of containers for development, testing,
and deployments.

## Objective

We'll modify the project `.gitlab-ci.yml` file to accomplish the following:
   * start using GitLab CI/CD stages
   * start building (compiling) the Go application
   * use the `Dockerfile` created earlier for building a container image
   * push the container image to GitLab's container registry

## The Updated .gitlab-ci.yml

Here is the updated `.gitlab-ci.yml`. Update your GitLab repository clone
with these changes. After saving, committing, and pushing the changes to
the remote GitLab repository, observe the GitLab CI/CD pipeline process
that gets triggered and make sure everything worked correctly. An explanation
of the changes made to the `.gitlab-ci.yml` file will follow below.

```yaml
image: golang:1.9

services:
  - docker:dind

stages:
  - lint
  - test
  - build
  - dockerize

before_script:
  - make deps

lint:
  stage: lint
  script:
    - make lint

test:
  stage: test
  script:
    - make test

build:
  stage: build
  script:
    - make build-linux
  artifacts:
    paths:
      - luka
    expire_in: 1 hour

dockerize:
  stage: dockerize
  image: docker:latest
  # override the global before_script list
  before_script: []
  # depend on the build stage to access its artifact
  dependencies:
    - build
  # only run this job on the "master" branch
  only:
    - master
  script:
    - docker build -t $CI_REGISTRY_IMAGE .
    - docker login $CI_REGISTRY -u gitlab-ci-token -p $CI_BUILD_TOKEN
    - docker push $CI_REGISTRY_IMAGE
```

## .gitlab-ci.yml Changes Explained

### Services

We elect to use the `docker:dind` ("dind" = "Docker in Docker") service so
that we can perform Docker related chores during the CI/CD process.

[GitLab services documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service)


### Stages

Previously, the linting and testing jobs both ran within what GitLab considers
to be a single stage. A stage can have multiple jobs and it can be helpful to
divide up work into stages for logical separation. The modified
`.gitlab-ci.yml` file now has four stages:
  * lint
  * test
  * build
  * dockerize

The existing two jobs have remained unchanged except for receiving stage
labels, and two new stages have been added for the building of the Go
application and for the creation and pushing of a Docker container image.

Stages will run linearly in the order they are listed in the YAML file. Our
file only specifies a single job per stage, but if there were multiple jobs
defined, they would be ran in parallel.

[GitLab stages documentation](https://docs.gitlab.com/ee/ci/yaml/#stages)  


### Artifacts

The new `build` stage, which compiles the Go application into a `luka` binary,
defines the `luka` binary as an artifact with an expiration time of 1 hour.

The artifact will be persist across stages and this is very important because
the `build` stage uses one container image capable of running Go and the
`dockerize` stage that follows uses a separate container image capable of
running Docker.

[GitLab artifacts documentation](https://docs.gitlab.com/ee/ci/yaml/#artifacts)  


### Images

At the top level, a default `golang:1.9` image is defined for all stages and
in the `dockerize` stage this default is overridden with a separate
`docker:latest` image. The use of a `docker` image in conjunction with the
`docker:dind` service that we use is not the only way to perform work with
containers. See the GitLab guide on building images for more options.

[GitLab building Docker images documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)  
[GitLab images documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)


### only

The new `build` stage uses an `only` parameter set equal to a list of Git
branches that the stage will be ran for. Only the `master` branch is on the
list, so the stage will only be ran when commits are made to the `master`
branch. This type of scoping can be very helpful in the CI/CD process. In our
project now, commits made to a feature branch will not trigger the container
building and pushing.


### before_script

The top level `before_script` section defines tasks to be ran as prep work for
each stage. This existed in the previous version of the `.gitlab-ci.yml` file
that we used. But in this version, the new `dockerize` stage overrides the
default `before_script` definition by defining its own (empty) definition.
Not only will the `dockerize` stage not need to run the `make deps` task
because it is only dealing with the compiled binary artifact, but it also
cannot run the executable commands involved because it uses a Docker image
instead of a Go one.

[GitLab before_script and after_script documentation](https://docs.gitlab.com/ee/ci/yaml/#before_script-and-after_script)  


### Dependencies

The new `dockerize` stage declares a dependency on the `build` stage, to ensure
that the compiled Go binary `luka` is available.

[GitLab dependencies documentation](https://docs.gitlab.com/ee/ci/yaml/#dependencies)  


### Variables

The new `dockerize` stage handles the Docker commands to both build and push
an image based on the project's `Dockerfile` file. The commands that are ran
make use of the following variables:
  * CI_REGISTRY_IMAGE - the image URL, including the container registry address, the image name, and the "latest" tag
  * CI_REGISTRY - the container registry address
  * CI_BUILD_TOKEN - the build token used to authenticate against the container registry

[GitLab's variables documentation](https://docs.gitlab.com/ee/ci/variables/)  


**Previous Page:** [Incorporating Containers In the Development Process](./03b_incorporating_containers_in_the_development_process.md)  
**Next Page:** [Getting Started With Kubernetes](./04a_getting_started_with_kubernetes.md)  
