**Previous Page:** [Incorporating Containers In the CI/CD Process](./03c_incorporating_containers_in_the_cicd_process.md)  
**Next Page:** [Deploying With Kubernetes Locally](./04b_deploying_with_kubernetes_locally.md)  

# Getting Started With Kubernetes

## What Kubernetes is

[Kubernetes](https://kubernetes.io), or k8s for short (using an "8" to replace
the middle eight characters), is an open source software system providing
container based deployment, scaling, and management functionality. Kubernetes
was originally developed by Google engineers borrowing from and improving upon
Google's long-running Borg platform for managing deployments for all of
Google's cloud hosted products. The official 1.0 release of Kubernetes came out
on July 21st, 2015 and was released by the
[Cloud Native Computing Foundation](https://www.cncf.io) (CNCF), which was
formed by the partnering of Google and the Linux Foundation at the time.

Kubernetes automates the clustering of containers (using images from Docker and
others) and provides standardized approaches for all of the following:
  * [Service discovery and load balancing](https://kubernetes.io/docs/concepts/services-networking/service/)
  * [Storage orchestration](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)
  * [Automated rollouts and rollbacks](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
  * [Batch execution](https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/)
  * [Automatic binpacking](https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/)
  * [Self-healing](https://kubernetes.io/docs/concepts/workloads/controllers/replicationcontroller/#how-a-replicationcontroller-works)
  * [Secret and configuration management](https://kubernetes.io/docs/concepts/configuration/secret/)
  * [Horizontal scaling](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)

Kubernetes is supported by the Amazon Web Services (AWS), Microsoft Azure, and
Google Cloud Platform (GCP) cloud providers. 


## Concepts and Components

Kubernetes offers an extremely comprehensive, well thought out,
self-contained system and as such it can take a user some time to fully
familiarize himself or herself with all of the system's concepts and parts.

The official documentation is a great place to start for coming up to speed.
In particular, see the following two sections:

* [Kubernetes Concepts](https://kubernetes.io/docs/concepts/)
* [Kubernetes Components](https://kubernetes.io/docs/concepts/overview/components/)


## kubectl

[kubectl](https://kubernetes.io/docs/reference/kubectl/kubectl/) is the
defacto standard command line tool used to interact with Kubernetes clusters.

kubectl works by performing network calls to a cluster using the
[Kubernetes API](https://kubernetes.io/docs/concepts/overview/kubernetes-api/).

Even when using tools such as Red Hat's [OpenShift](https://www.openshift.com),
SAP's [Gardener](https://gardener.cloud), or AWS'
[EKS](https://aws.amazon.com/eks/), the kubectl can still be used to directly
interact with the Kubernetes clusters involved.


## Minikube

[Minikube](https://kubernetes.io/docs/setup/minikube/) is a software tool that
uses a single node to host a Kubernetes cluster locally within a virtual
machine. Minikube allows the developer to iterate quickly and affordably using
a local instance of Kubernetes with no customer traffic affected by outages and
configuration changes. We'll make use of Minikube for the next section to go
hands on with Kubernetes.


## Installing Kubernetes and Minikube

To install kubectl, follow the Kubernetes homepage's
[installation instructions](https://kubernetes.io/docs/tasks/tools/install-kubectl/).

For macOS users with Homebrew, perform `brew install kubernetes-cli`
to install kubectl.

To install Minikube, follow the Minikube source repository's
[installation instructions](https://github.com/kubernetes/minikube#installation).

For macOS users with Homebrew Cask, perform `brew cask install minikube` to
install Minikube.


**Previous Page:** [Incorporating Containers In the CI/CD Process](./03c_incorporating_containers_in_the_cicd_process.md)  
**Next Page:** [Deploying With Kubernetes Locally](./04b_deploying_with_kubernetes_locally.md)  
