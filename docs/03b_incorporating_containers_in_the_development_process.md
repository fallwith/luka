**Previous Page:** [Getting Started With Docker](./03a_getting_started_with_docker.md)  
**Next Page:** [Incorporating Containers In the CI/CD Process](./03c_incorporating_containers_in_the_cicd_process.md)  

# Incorporating Containers In the Development Process

By leveraging containers as part of his or her workfflow, the developer can
expect to experience the following pros and cons:

### Pros
* **Quick, easy, reliable setup** - With all of various parts required to run
                                  an application such as a web server,
                                  programming language virtual machine,
                                  databases, etc. containerized, the developer
                                  does not have to worry about getting all of
                                  those running on their own workstation.
* **Consistency** - By developing and testing against the very same container
                  used by other developers and used in deployments, the
                  developer can be assured of consistency in the behavior they
                  observe with their local instance of a container.

### Cons
* **Complexity** - If a developer wishes to place a breakpoint in running code
                 or directly access a log file or get directly at a software
                 depenency, the extra abstraction layer of a container can
                 make things more complicated.
* **Resources** - The container server itself takes up system resources which
                above whatever the containerized content itself requires.

In most cases - especially when containers are being used for deployments -
the developer should incorporate containers for some part of their development
workflow even when the cons prevent their usage at all times.

For example, a developer may choose to eschew containers for complex code
debugging but then switch to containers for integration testing. Containers
also make for great baselines. If one developer has an issue, he or she could
make sure to be able to reproduce it in a container prior to seeking
assistance to rule out something specific to the developer's own machine.

We'll focus on creating a single container for the web application we've built
and leverage it for integration testing with Curl just as before.


## The Dockerfile

A `Dockerfile` file is a recipe followed top to bottom for building a
container image.

For the web application, create a `Dockerfile` file in the same directory as
`main.go` source file with the following content:

```shell
# Dockerfile
FROM ubuntu:18.04

WORKDIR /app
COPY luka /app

EXPOSE 8080

CMD ["/app/luka"]
```

* **FROM <IMAGE>** - This specifies the base container image to use, typically
                   hosted by the public Docker image repository. For this guide
                   we'll be compiling the Go web app into a Linux binary and
                   choosing an Ubuntu v18.04 Linux image as our base.
* **WORKDIR <PATH>** - This sets the present working directory within the
                     container used for image creation for the steps to
                     follow, and creates that directory if it does not yet
                     exist.
* **COPY <FROM> <TO>** - This copies a file from the workstation running Docker
                       to the container being used to construct an image.
* **EXPOSE <PORT>** - This exposes the given network port number to traffic.
* **CMD <COMMAND>** - This executes the given command as the primary purpose
                    of the container. Note that this command is expected to
                    run indefinitely and that a running container will stop
                    functioning when the command stops.


## Building a container

With the `Dockerfile` in place, we need to perform the following:

* compile the Go source code as a Linux binary
* build a Docker container image with the Linux binary baked in

Use the following commands to carry out these steps:

```
env GOOS=linux go build -o luka main.go
docker build -t luka .
```

By setting the `GOOS` (Go OS) environment variable to `Linux`, we instruct the
Go compiler to produce a Linux binary. The `-o luka` argument creates the
Linux binary with a filename of `luka`.

The `docker build` command receives two arguments as input. The `-t luka`
argument instructs Docker to tag the created image with the string `luka`.
Note that this string does not need to match the name of the Linux binary and
that they just so happen to match in this case where the `luka` string is being
used for everything. The other, final argument passed to `docker build` is a
period (`.`), signifying the current working directory. This directory must
contain a `Dockerfile` file for the `docker build` command to work, and it
must contain a binary file named `luka` for the `Dockerfile`'s `COPY` command
to work.

Once the container image is built, it - and the base Ubuntu image it's based
on - will show up in the Docker images list:

```shell
ocker images
REPOSITORY  TAG     IMAGE ID      CREATED         SIZE
luka        latest  5949473c45c3  18 minutes ago  98.8MB
ubuntu      18.04   735f80812f90  12 days ago     83.5MB
```


## Running a container

To run a container based on the newly created image, run the following:

```shell
docker run --rm -itp 8080:8080 luka
```

Here is the breakdown of the arguments passed to the `docker run` command:

* **--rm** - After running the container, remove it. Without this, a new
             container would be spawned every time the `run` command was ran and
             the old ones would need to be pruned manually.
* **-i** - Run the container interactively. This will allow for us to use the
           host machine to issue a CTRL-C interrupt to halt the running
           container.
* **-t** - Allocates a psuedo TTY to have the container's output messages reach
           the host machine's terminal.
* **-p <HOST PORT>:<CONTAINER PORT>** - Publish the container's port to the
                                        host's port. The port numbers do not
                                        need to match as they do in this
                                        example.
* **<NAME>** - The image name to run a container based on. In our example we
               we use `luka`, which corresponds to the `-t luka` argument
               passed to `docker build` earlier.


## Interacting with a container

With the container running, we can use a separate shell session to interact
with the web app running inside the container via Curl just as we did before
when running the web app via `go run`.

```shell
% curl http://localhost:8080
luka 0.0.1%

% curl http://localhost:8080/greet/Luka
Hello, Luka%
```

The running container is running a Ubuntu Linux and can be interacted with
accordingly. In a shell session separate from the one running the Docker
container, obtain the running container's id and then interact with that
running container by launching an interactive Bash shell session on it:
```shell
# Use 'docker ps' (optionally with 'awk' if you have it) to determine the
# running container's id
docker ps
# or
docker ps | grep luka | awk '{print $1}'

# Launch an interactive Bash shell on the container, run some
# commands, and exit
docker exec -it 2ae7b7b4f1dd bash
bash> ls -l /
bash> ps ef
bash> exit
```

Once you have finished interacting with the running Docker container, input
`CTRL-C` (hold down the `CTRL` keyboard key and then hit the `C` key) in the
shell session running the container to stop it.

## Removing Docker images

To remove the Docker image we created for the web application, we can run
the following:
```shell
docker rmi luka
```

The `docker rmi` (remove image) command accepts an image name as input.

We can run the `docker images` command again to confirm that the `luka` image
has been removed.
```shell
docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              18.04               735f80812f90        12 days ago         83.5MB
```

If you'd like to get rid of the Ubuntu base image as well, run the following:
```shell
docker rmi ubuntu:18.04
```


## Makefile enhancements

In this section we've learned to build the Go code into a Linux binary,
how to package everything up as a Docker container image, how to run that
image as a container, and how to clean up afterwards.

Let's update the `Makefile` file to define new targets to handle all of that
functionality automatically. These are the new commands we'll be adding
support for:

* `make build-linux`: Build the Go code into a Linux executable
* `make docker-build`: Build a Docker container image
* `make docker-clean`: Clean up after Docker
* `make docker-run`: Run a Docker container

The updated `Makefile` content is as follows:

```Makefile
# Makefile
build: deps
	go build -o luka main.go
build-linux: deps
	env GOOS=linux go build -o luka main.go
clean:
	rm -f luka
curl:
	curl http://localhost:8080
	curl http://localhost:8080/greet/Luka
deps:
	go get github.com/gin-gonic/gin
	go get github.com/alecthomas/gometalinter
	gometalinter --install
docker-build: build-linux
	docker build -t luka .
docker-clean: clean
	docker rmi luka
docker-run: docker-build
	docker run --rm -itp 8080:8080 luka
lint:
	gometalinter --deadline=5m ./...
run: deps
	go run main.go
test: deps
	go test -v ./...
```


**Previous Page:** [Getting Started With Docker](./03a_getting_started_with_docker.md)  
**Next Page:** [Incorporating Containers In the CI/CD Process](./03c_incorporating_containers_in_the_cicd_process.md)  
