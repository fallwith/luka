**Previous Page:** [Getting Started With Kubernetes](./04a_getting_started_with_kubernetes.md)  
**Next Page:** [The Kubernetes Dashboard](./04c_the_kubernetes_dashboard.md)  

# Deploying With Kubernetes Locally

## Overview

With `kubectl` and Minikube installed, we'll test a Kubernetes based deployment
of the Go web app authored previously.

While the `minikube` commands will apply only to the local Minikube software,
all of the `kubectl` commands will remain valid when used with Kubernetes
clusters outside of Minikube later.


## Start Minikube

Start Minikube. During the first time Minikube is ran, it will fetch an image
to power the virtual machine and download and install various prerequisites.
This process can take some time.

```shell
minikube start
```

If you ever want to peek inside the running Minikube VM, you can do so with
`minikube ssh`.

Once Minikube has stared, use `kubectl` to make sure everything
is working correctly.

```shell
# make sure you can connect to Minikube without any errors reported
kubectl cluster-info

# gather information on all active Kubernetes components. at a minimum,
# kubernetes itself should be listed
kubectl get all
```


## Minikube's Docker registry

Minikube will utilize its own Docker registry. This means that it won't be able
to fetch any images that you mave have previously built or pulled down locally.

In order to interact with Minikube's own registry, issue the following
command via a unix terminal session:

```shell
eval $(minikube docker-env)
```

That command will export the appropriate environment variables to have Docker
interact with Minikube's registry. You can observe what the environment
variables are by running `minikube docker-env` without the `eval`.

Now that you have a shell session with Docker configured to utilize Minikube's
registry, it's time to get the Luka Docker image into the Minikube registry.

There are two ways to go about this. You can fetch the image pushed to
the GitLab container registry via the GitLab CI/CD pipeline, or you can
build a fresh local container. Follow the desired path below.


## Option 1: Fetch the GitLab registry hosted container image

Previously we set up a GitLab CI/CD pipeline to automatically build a Docker
image and push it to GitLab's own container registry.

We'll now fetch the image from the GitLab container registry and have a copy
of it stored locally in Minikube's own registry.

First, we'll need to create a read only access "Deploy Token" for use with
fetching GitLab container based images.

In a web browser, head to your GitLab project repository page. Then from
the Repository settings section, define a new read-only registry Deploy Token:
  * (project page) -> Settings -> Repository -> Deploy Tokens -> Expand
  * Define a name and expiration date, checkmark read_registry, and click Create
  * don't close the browser window!

After creating the new Deploy Token, look for the `Your New Deploy Token`
section that has appeared on the page above where you filled out the token's
information. Write down the username and password values shown. This will be
your only chance to obtain these credentials (though you can delete the token
and create another one if necessary).

Using a unix shell session that has been prepped to interact with the
Minikube registry (as explained above), use Docker to fetch the GitLab
registry hosted image and have a copy stored in the Minikube registry.

```shell
# replace <TOKEN USERNAME> and <TOKEN PASSWORD> with the correct
# values for your GitLab Deploy Token
docker login registry.gitlab.com -u <TOKEN USERNAME> -p <TOKEN PASSWORD>

# replace <GITLAB USERNAME> and <GITLAB REPO NAME> with your GitLab username
# and the GitLab repository name
docker pull registry.gitlab.com/<GITLAB USERNAME>/<GITLAB REPO NAME>
```


## Option 2: Build a new Docker image

Using the shell session that was configured to reference Minikube's Docker
registry, build a new image in the same way as was done previously against
the local (non-Minikube) registry.

NOTE: this command should be ran within the directory containing the project
`Dockerfile` file.

```shell
docker build -t luka .
```

## Use the Luka image to create a Kubernetes cluster

NOTE: continue to use the same unix shell session with the environment
variables exported to configure Docker to reference the Minikube container
registry.

After following one of the two options above, your Minikube Docker registry
should now have a copy of the Luka container image. You can confirm this by
running `docker images`.

Now it is time to use the Luka image as the basis for a Kubernetes cluster.

```shell
# option 1 approach:
kubectl run luka --image=registry.gitlab.com/<GITLAB_USERNAME>/<GITLAB REPO NAME> --port=8080 --generator=run/v1 --image-pull-policy=Never

# option 2 approach:
kubectl run luka --image=luka --port=8080 --generator=run/v1 --image-pull-policy=Never
```

The `kubectl` command will interact with the Minikube hosted Kubernetes
instance to generate a replication controller which will in turn automatically
handle the spawning of new pods for scaling.

The following explains the individual parts of the command issued:
  * `run`: runs a command
  * `--image=<IMAGE>`: the name of the container image to use
  * `--port=8080`: the port the containers based on the image will listen on
  * `--generator=run/v1`: essentially this tells Kubernetes to create a replication controller
  * `--image-pull-policy=Never`: disable fetching of updated images

Next, wait for Kubernetes to provision the desired replication controller.

```shell
kubectl get replicationcontrollers

# or

kubectl get rc
```

Once the replication resource is listed as `READY`, proceed to wait for it
to spawn a pod to run the `luka` container.

```shell
kubectl get pods
```

You should see a pod with a name starting with "luka-". Continue to run the
same command until that pod shows `1/1` in the `READY` column and `Running` in
the `STATUS` column.

`kubectl` can also yield more detailed information about a resource via
the use of `kubectl describe`.

```shell
# produces more detailed information than "kubectl get"
kubectl describe pods
```

By default, the pods spawned by the replication controller will be private to
the Kubernetes cluster and not reachable outside of it. We'll now expose the
replication controller's resources by creating a new load balancer service.

```shell
kubectl expose rc luka --type=LoadBalancer --name luka-lb
```

Take a look at the new load balancer service via `kubectl`:

```shell
kubectl get service/luka-lb
```

Note that the `EXTERNAL-IP` value should be `<pending>`. When working with a
production Kubernetes cluster, you'll want that `<pending>` value to eventually
be replaced with an ip address value. But while using Minikube, the value will
stay at `<pending>` indefinitely.

With a pod and a load balancer in place, we can now access the `luka` web
application that is running inside of our Kubernetes cluster.

The `kubectl get service/luke-lb` command output yielded the ip address for the
Kubernetes cluster itself, but not the usable Minikube ip that we need. The
Minikube ip address can be obtained via:

```shell
minikube ip
```

Armed with the Minikube ip address and the port number from the
`kubectl get service/luka-lb` command (the port that is not `8080`), we can
use cURL to access the running web application. But Minikube provides us with
an easier solution that does not require manually cobbling together values from
from two different commands.

```shell
minikube service luka-lb --url
```

The `minikube service <service name> --url` command will yield a valid external
facing URL for accessing the service. Note that leaving off the `--url` switch
will cause Minikube to launch the default web browser and connect to the
service.

With the output of this `minikube service <service name> --url` command, we can
now run cURL tests against the running Luka web application.

```shell
# access / for the web app via cURL
curl `minikube service luka-lb --url` -w "\n"

# access the /greet/<name> endpoint
# curl `minikube service luka-lb --url`/greet/luka -w "\n"
```

Note that the `-w "\n"` argument is intended to append a newline on the end
of the cURL output and is not necessary for the test to work.


## Scaling the pod cluster

Our cluster's replication controller spawned a single pod for us to run the
web application container. This is the default behavior. Let's practice
scaling up a Kubernetes pod cluster by instructing our replication controller
to spawn a second pod for us.

```shell
kubectl scale rc luka --replicas=2
```

Now when we next issue a `get` or `describe` command for our pods, we'll see
a second pod in the cluster.

```shell
# two pods should now be listed with names beginning with "luka-"
kubectl get pods
```


We now have our Go web application running and scalable in a Kubernetes
cluster! In the next section we'll take a look at the Kubernetes dashboard to
see how things look from a web UI and then go over how to stop and optionally
clean up the cluster.

**Previous Page:** [Getting Started With Kubernetes](./04a_getting_started_with_kubernetes.md)  
**Next Page:** [The Kubernetes Dashboard](./04c_the_kubernetes_dashboard.md)  
