**Previous Page:** [Getting Started With GitLab](./02a_getting_started_with_gitlab.md)  
**Next Page:** [Getting Started With Docker](./03a_getting_started_with_docker.md)  

# Triggering Tests and Linting With GitLab

## Create a Git repository

From the [Projects](https://gitlab.com/dashboard/projects) section of your
GitLab dashboard, create a new project:

* Give the project a name. You may use "luka" or whatever else you'd like
* Choose any of the listed "Visibility Level" options
* Checkmark the "Initialize repository with a README" option unless you are
  sufficiently comfortable with initializing a GitLab repo on your own


## Add the Go code, tests, and Makefile to the repository

If you are already comfortable using Git, do the following:

* Clone the new repository
* Add the `main.go`, `main_test.go`, and `Makefile` files to the repository via
  your local clone
* Commit and push the files to the remote repository, either by leveraging
  a feature branch and GitLab's pull request system to merge the changes into
  the default `master` branch, or by targetting the `master` branch directly

If you are not comfortable with Git or would simply prefer to use the GitLab
web interface to add files, do the following:

* By using the plus sign `+` drop down menu next to the project name, choose
  `Upload file` and follow the instructions that appear to upload the `main.go`
  file
* Repeat the process to upload the `main_test.go` and `Makefile` files as well


## Create a .gitlab-ci.yml file in the repository

GitLab stores each repository's continuous integration configuration in a
`.gitlab-ci.yml` [YAML](http://yaml.org) file.

GitLab.com hosts a
[Getting started with GitLab CI/CD](https://gitlab.com/help/ci/quick_start/README)
guide that provides a complete overview of the functionality.

Let's author a `.gitlab-ci.yml` file to accomplish the following tasks -
covered previously while developing the Go web application -
every time a change is made to the repository:

* Run the suite of Go unit tests with `go test`
* Run a Go Meta Linter sweep over the code

Both of these tasks have already been addressed via targets in the
`Makefile` file, and we'll be leveraging those targets now at the CI
layer.

Having the developer run the exact same commands locally during the development
process as are ran during CI provides for great consistency and
maintainability, because the commands only need to be maintained in one place.

Add the following content to a local file named `.gitlab-ci.yml`:

```yaml
image: golang:1.9

before_script:
  - apt-get update -qq && apt-get install -y -qq golang
  - make deps

lint:
  - make lint

test:
  - make test
```

Our `.gitlab-ci.yml` file contains the following:

* An image specification
* A specially named `before_script` section. GitLab CI recognizes this section
  name and will process it first before all other sections.
* Custom `lint` and `test` sections that we've named with whatever names we
  wanted to use and any commands we wanted to define.

By default, GitLab's CI system will use an Ubuntu Linux based image. That
image does not contain Go and if instructed to fetch Go may fetch an older
version of Go that does not support our source code. By specifying the
(publicly available via the GitLab image repository) `golang` image with the
desired version number of `1.9`, we can be assured of having our desired Go
version on hand and save the time that would otherwise be spent on adding
Go to a vanilla Ubuntu image.

The `before_script` section will use our `Makefile`'s `deps` target to get
the project's dependencies satisfied.

Next, the `lint` section will invoke the `lint` target to run a Go Meta
Linter sweep across the code and then the `test` section will invoke the
`test` target, which will run our Go unit tests defined in `main_test.go`

Again following the process used previously to add the original 3 files to the
repository, add the `.gitlab-ci.yml` file to the repository.

NOTE: Because `.gitlab-ci.yml` begins with a period, it is recognized as a
hidden file and as a result might not be easy to spot on your machine after
saving it. The macOS Finder application requires you to hit
`CMD + Shift + .` to show hidden files, for example. If you would like to use
the GitLab web interface to upload the file and are having trouble spotting it
for the drag and drop process, you may instead elect to author the file
directly via the GitLab web interface. To do so, choose `New file` from the
plus sign `+` drop-down menu instead of `Upload file`.

Once you've added the `.gitlab-ci.yml` file to the GitLab repository, head to
the GitLab's web based dashboard for the project and click on the `CI / CD`
link to access the GitLab CI dashboard.

If all has gone well, you'll notice a new "pipeline" with a "running" status.
Once the pipeline has completed, the status should change from "running" to
either "passed" or "failed".

Now when each new change is applied to the repository, a new pipeline will
start and automatically run our Go unit tests and linter sweep.


**Previous Page:** [Getting Started With GitLab](./02a_getting_started_with_gitlab.md)  
**Next Page:** [Getting Started With Docker](./03a_getting_started_with_docker.md)  
