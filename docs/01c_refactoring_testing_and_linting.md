**Previous Page:** [Writing a Web App With Go](./01b_writing_a_web_app_with_go.md)  
**Next Page:** [Getting Started With GitLab](./02a_getting_started_with_gitlab.md)  

# Refactoring, Testing, and Linting

Here again is the Go code written on the previous page:
```go
// main.go
package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.String(200, "luka 0.0.1")
	})
	router.GET("/greet/:name", func(c *gin.Context) {
		c.String(200, "Hello, "+c.Param("name"))
	})
	router.Run()
}
```

There are at least the following issues with that code:

* It is difficult to test.
  * A Go unit test won't be able to directly invoke the
    `main()` function shy of running the app in binary form.
  * The behavior for the web app's responses is defined inline within the
    route definitions, making it impossible to know what the route is meant
    to do without lighting it up through invocation.
* It is difficult to maintain.
  * As more routes are added, the inline defining of their functions
    all in one place will grow to be unwieldy.
  * The program name (`luka`) and version (`0.0.1`) strings are defined
    within the default route function instead of a separate dedicated spot
    where they can be found and updated if necessary.
* It generates warnings when ran through linters.
  * At the time of this writing, the `gas` and `errcheck` Go linters generate
    a single warning each when processing the code.

To observe firsthand the issues that the linters find with the code, prep
Go Meta Linter via the instructions on the [Getting Started With Go page](./01a_getting_started_with_go.md) (in the Confirmation section) and then run the
`gometalinter` binary against `main.go`:
```shell
gometalinter --deadline=5m main.go

# alternatively, lint everything starting from the current directory
gometalinter --deadline=5m ./...
```

NOTE: the `--deadline=5m` argument will extend the amount of time that Go
Meta Linter waits for all linters to finish to 5 minutes. This extension
can be helpful when running on machines or containers that don't allow for
much concurrency or are slow.

We will now take steps to refactor the code, add unit tests for it, and make
sure that it does not generate any linter warnings.

## Refactoring and Linting

### Part 1 - Move content out of main()

The original code has all of its functionality defined in the not easily
testable `main()` function. Let's start the refactoring process by simply
getting that content out into a separate function that can be tested.

```go
// OLD
func main() {
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.String(200, "luka 0.0.1")
	})
	router.GET("/greet/:name", func(c *gin.Context) {
		c.String(200, "Hello, "+c.Param("name"))
	})
	router.Run()
}

// NEW
func getRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.String(200, "luka 0.0.1")
	})
	router.GET("/greet/:name", func(c *gin.Context) {
		c.String(200, "Hello, "+c.Param("name"))
	})
	return router
}

func main() {
	getRouter().Run()
}
```

The router object is now defined and returned by a new `getRouter()` function.
You may wonder how to determine the correct return type of `*gin.Context` for
the new function to use. There are a few ways to do so:

* Check out the imported package's documentation and source code
* Define the function as returning a known wrong type and have Go complain
  about a mismatch between the expected and actual return types (exposing
  the actual type in the process)
* Temporarily mport the `fmt` package and leverage its `%T` (type)
  functionality: `fmt.Println(fmt.Sprintf("The type is %T", router))`
* Temporarily Import the `reflect` package which provides code reflection
  functionality at runtime:
  `fmt.Println("The type is", reflect.TypeOf(router).String())`


### Part 2 - Use a dedicated function for each route

The approach of having every route's handling functionality defined via
inline `func()` blocks all in the same spot can quickly become complicated.
Defining all of the route paths in one spot and the handlers for those paths
elsewhere is best for separation of purpose and maintainability. Some web apps
even go as far as to have the route paths defined in a file all by themselves.

```go
// OLD
func getRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.String(200, "luka 0.0.1")
	})
	router.GET("/greet/:name", func(c *gin.Context) {
		c.String(200, "Hello, "+c.Param("name"))
	})
	return router
}

// NEW
func defaultHandler(c *gin.Context) {
	c.String(200, "luka 0.0.1")
}

func greetHandler(c *gin.Context) {
	c.String(200, "Hello, "+c.Param("name"))
}

func getRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/", defaultHandler)
	router.GET("/greet/:name", greetHandler)
	return router
}
```

### Part 3 - Separation

Currently the route handler functions define their text responses inline. Let's
update them to invoke functions instead to obtain their response values. This
will allow us to test these responses better by invoking both the handler
and the separate function for getting a response string and making sure their
output matches.

While we're doing this refactor, let's also move the application name and
version out into variables, making their purpose more clear and their future
maintainance easier.

```go
// OLD
func defaultHandler(c *gin.Context) {
	c.String(200, "luka 0.0.1")
}

func greetHandler(c *gin.Context) {
	c.String(200, "Hello, "+c.Param("name"))
}

// NEW
const appName = "luka"
const appVersion = "0.0.1"

func greeting(name string) (g string) {
	return "Hello, " + name + "!"
}

func appInfo() (g string) {
	return appName + " " + appVersion
}

func defaultHandler(c *gin.Context) {
	c.String(200, appInfo())
}

func greetHandler(c *gin.Context) {
	name := c.Param("name")
	c.String(200, greeting(name))
}
```

### Part 4 - Satisfy the linters

Running the Go Meta Linter against the refactored code will - at the time of
this writing - yield a single remaining error:
```shell
% gometalinter --deadline=5m main.go
main.go:35:17:warning: error return value not checked (getRouter().Run()) (errcheck)
```

This warning is referring to the fact that the `Run()` call being made in
`main()` returns an error return type and we are not bothering to check
the return value. We can fix up `main()` like so:
```go
// OLD
func main() {
	getRouter().Run()
}

// NEW
func main() {
	err := getRouter().Run()
	if err != nil {
		panic("Error encountered while serving: " + err.Error())
	}
}
```

### Part 5 - The complete refactored code

Here is the complete version of `main.go` after all of the refactorings have
been applied.

```go
package main

import (
	"github.com/gin-gonic/gin"
)

const appName = "luka"
const appVersion = "0.0.1"

func greeting(name string) (g string) {
	return "Hello, " + name + "!"
}

func appInfo() (g string) {
	return appName + " " + appVersion
}

func defaultHandler(c *gin.Context) {
	c.String(200, appInfo())
}

func greetHandler(c *gin.Context) {
	name := c.Param("name")
	c.String(200, greeting(name))
}

func getRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/", defaultHandler)
	router.GET("/greet/:name", greetHandler)
	return router
}

func main() {
	err := getRouter().Run()
	if err != nil {
		panic("Error encountered while serving: " + err.Error())
	}
}
```

### Part 6 - Etc.

Refactoring can be a subjective thing, and therefore can be done indefinitely
as long as different perspectives exist.

If you are so inclined, feel free to apply whatever additional refactorings
you'd like.

Perhaps one of these:

* Import `net/http` and use `http.StatusOK` in lieu of the literal `200` value
* Both routes have the same response code and a plain text string response
  body. Perhaps a function can be defined that both route handlers call in
  order to cut down on the code duplication.
* The greet route greets the given name, so perhaps a non blank name parameter
  should be required.
* Perhaps some aspect of the code violates a code style guide. For example,
  many gophers (Go coders) would prefer to use `r` as a variable name instead
  of `router`.
* Use a custom type of greeting depending on the name supplied - maybe even
  using different languages. This type of change can be made now that the
  previous refactorings have been applied.



## Testing

Now that the code has been refactored, it lends itself well to being tested
with Go unit tests. We'll author some now.

NOTE: While this guide does not currently follow a test driven development
(TDD) approach, it is not meant to recommend against using one.

Given that the two defined routes are both accessed via a URI using an
HTTP method and expected to return a string body, the tests for them will
be very similar. Rather than duplicate the same logic, we'll use a helper
function and have both routes leverage the function.

Here is the complete set of tests, annotated with source code comments. Save
this code as `main_test.go` in the same directory that `main.go` resides in.

```go
// main_test.go

// the package name should match that defining the functionality being tested
package main

import (
	// ioutil is used here to read response bodies
	"io/ioutil"
	// http and httptest are used here for HTTP testing
	"net/http"
	"net/http/httptest"
	// testing is the built-in Go testing package
	"testing"
)

// This is a helper function that all route tests can leverage.
// If this helper would be of use to other files containing tests, it would
// typically be moved out into a dedicated helpers file.
//
// receives:
//   t            - the Go unit test object
//   method       - the HTTP method (GET, POST, etc.) to use
//   uri          - the URI to test
//   expectedBody -
//
// returns:
//   (none) - the function directly use the `t` object to generate test errors
func sendTestRequest(t *testing.T, method string, uri string, expectedBody string) {
	// With `main.go`'s `getRouter()` exposing the Gin router object, the
	// test can get at it
	r := getRouter()
	// Use Go's built-in HTTP testing functionality to define a request and
	// then use the router instance to service it
	rec := httptest.NewRecorder()
	req, _ := http.NewRequest(method, uri, nil)
	r.ServeHTTP(rec, req)
	// Expect the call to return with an "OK" HTTP code
	if rec.Code != http.StatusOK {
		t.Errorf("Expected a success from the '" + uri + "' uri, but received an error")
	}
	// Parse the response body and make sure it matches what the helper function
	// was told it should be
	body, _ := ioutil.ReadAll(rec.Result().Body)
	if string(body) != expectedBody {
		t.Errorf("Expected the response body to match '" + expectedBody +
			"', but got '" + string(body) + "'")
	}
}

// Test the default (/) route
func TestDefaultRoute(t *testing.T) {
	// With `appInfo()` dedicated to yielding a string with the app name and
	// version, it can be invoked by the testing layer
	expectedBody := appInfo()
	// Call the helper function with default route specific values to do the
	// actual testing
	sendTestRequest(t, "GET", "/", expectedBody)
}

// Test the greet (/greet/:name) route
func TestGreetRoute(t *testing.T) {
	// Place a name in a variable and use that variable multiple times instead
	// of repeating the name string. This cuts down on programmer error and helps
	// with maintainability.
	name := "Indiana"
	// With the `greeting()` function yielding a greeting for a given name,
	// we can determine what the greeting should be prior to hitting the route
	expectedBody := greeting(name)
	// Call the helper function with the greeting route specific values to do
	// the actual testing
	sendTestRequest(t, "GET", "/greet/"+name, expectedBody)
}
```

At a command prompt the tests can be ran with `go test`.

To process all test files recursively from the present location, use the
`./...` argument. By default, passing in this argument will muffle most output
from the tests and only output minimal information. For increased verbosity,
the `go` command supports a `-v` switch. These combine to make
`go test -v ./...`


## Makefile Updates

Let's define new targets for testing and linting so that the additional new
commands can be used:

* `make lint`: Run the Go Meta Linter against the project
* `make test`: Run the Go unit tests

The updated `Makefile` content is as follows:

```Makefile
# Makefile
build: deps
	go build -o luka main.go
clean:
	rm -f luka
curl:
	curl http://localhost:8080
	curl http://localhost:8080/greet/Luka
deps:
	go get github.com/gin-gonic/gin
	go get github.com/alecthomas/gometalinter
	gometalinter --install
lint:
	gometalinter --deadline=5m ./...
run: deps
	go run main.go
test: deps
	go test -v ./...
```


**Previous Page:** [Writing a Web App With Go](./01b_writing_a_web_app_with_go.md)  
**Next Page:** [Getting Started With GitLab](./02a_getting_started_with_gitlab.md)  
