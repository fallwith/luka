**Previous Page:** [Deploying With Kubernetes Locally](./04b_deploying_with_kubernetes_locally.md)  
**Next Page:** [Stopping and Removing Kubernetes Resources](./04d_stopping_and_removing_kubernetes_resources.md)  

# The Kubernetes Dashboard

## Overview

Kubernetes offers a web user interface (UI) dashboard tool literally
called Dashboard with a capital D.

Dashboard can be used to glean information about a cluster, tweak settings, or
to take action such as restarting a pod or kicking off a new deployment.

Minikube automatically installs and serves up Dashboard. For production
deployments of Kubernetes clusters, however, Dashboard is not installed by
default.

For more information about how to install Dashboard and about Dashboard itself,
consult the [official Dashboard documentation](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/).


## Accessing Dashboard

When using Minikube, the easiest way to access Dashboard is with the
`minikube dashboard` command. Try this command now to view your cluster's
Dashboard tool in a web browser.

```shell
# open Dashboard in the default web browser
minikube dashboard

# output the URL to Dashboard but don't open a browser
minikube dashboard --url
```


## Using Dashboard

With Dashboard loaded in a browser, you should first arrive at the `Overview`
page which should show some nice green "100%" healthy status circles for the
pods and replication controllers.

Below the circles you'll be able to view the two pods, the replication
controller, and the load balancer service.

On the right side of each resource row are a pair of buttons - a `Logs` button
that looks like four lines of text, and an `Actions` button that is made up of
three dots stacked vertically.

If you were to run the cURL tests from the prevous section several times now
that there are two pods in the cluster, you should be able to view the logs
for each pod and see requests appear against both.

Now let's scale the number of pods back down to 1. Click on the `Actions`
button for the `luka` replication controller, click `Scale`, change the
desired pod count to `1` and click `OK`.

After doing so, the `Pods` value for the replication controller row should
change to `2/1` to indicate that there are two pods in the cluster with a
desired count of only one.

The Dashboard interface will not auto-refresh after the scale down completes,
so manually refresh the `Overview` page until only one pod is listed.

Next, let's alter the port which we can access the web app across. On the
Dashboard `Overview` page, locate the row for the `luka-lb` load balancer and
click on the `Actions` button. Then click `View/edit YAML`. In the YAML
editor window that pops up, locate the `nodePort` key/value YAML pair and
change the value to a new port number. Be sure to not use a port number that
is not already in use on your system. To avoid doing so, you may elect to use a
value in the default range Kubernetes uses for the `nodePort` value, which is
`30000 - 32767`. Click `OK` after changing the YAML.

The new port value should appear on the `Overview` page's row for the `luka-lb`
load balancer. Feel free to repeat the cURL tests from the previous section
to verify that the web app is still reachable after the port change.


## Conclusion

Feel free to poke around in Dashboard and to view things or take any other
desired actions as described in the [official Dashboard documentation](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/#deploying-containerized-applications).

When ready, proceed to the next section to shut down the Kubernetes cluster and
optionally delete it.


**Previous Page:** [Deploying With Kubernetes Locally](./04b_deploying_with_kubernetes_locally.md)  
**Next Page:** [Stopping and Removing Kubernetes Resources](./04d_stopping_and_removing_kubernetes_resources.md)  
