**Previous Page:** [Stopping and Removing Kubernetes Resources](./04d_stopping_and_removing_kubernetes_resources.md)  
**Next Page:** [Deploying to GCP](./05b_deploying_to_gcp.md)  

# Getting Started With Google Cloud Platform (GCP)

## Overview


**Previous Page:** [Stopping and Removing Kubernetes Resources](./04d_stopping_and_removing_kubernetes_resources.md)  
**Next Page:** [Deploying to GCP](./05b_deploying_to_gcp.md)  
