# luka

luka is a tutorial project used to introduce web application development
and operations concepts


## Table of Contents

The following sections are intended to be read in sequential order. Each
individual page is linked to the pages that come before and after it.

* Develop a simple web application that responds to routes
  * [Getting Started With Go](./docs/01a_getting_started_with_go.md)
  * [Writing a Web App With Go](./docs/01b_writing_a_web_app_with_go.md)
  * [Refactoring, Testing, and Linting](./docs/01c_refactoring_testing_and_linting.md)
* Setting up a continuous integration pipeline
  * [Getting Started With GitLab](./docs/02a_getting_started_with_gitlab.md)
  * [Triggering Tests and Linting With GitLab](./docs/02b_triggering_tests_and_linting_with_gitlab.md)
* Developing with containers
  * [Getting Started With Docker](./docs/03a_getting_started_with_docker.md)
  * [Incorporating Containers In the Development Process](./docs/03b_incorporating_containers_in_the_development_process.md)
  * [Incorporating Containers In the CI/CD Process](./docs/03c_incorporating_containers_in_the_cicd_process.md)
* Deploying a web app locally
  * [Getting Started With Kubernetes](./docs/04a_getting_started_with_kubernetes.md)
  * [Deploying With Kubernetes Locally](./docs/04b_deploying_with_kubernetes_locally.md)
  * [The Kubernetes Dashboard](./docs/04c_the_kubernetes_dashboard.md)
  * [Stopping and Removing Kubernetes Resources](./docs/04d_stopping_and_removing_kubernetes_resources.md)
* Deploying a web app to the cloud
  * Getting Started With Google Cloud Platform (GCP)
  * Deploying To GCP
* Infrastructure As Code
  * Getting Started With Terraform
  * Provisioning GCP With Terraform
