package main

import (
	"github.com/gin-gonic/gin"
)

const appName = "luka"
const appVersion = "0.0.1"

func greeting(name string) (g string) {
	return "Hello, " + name + "!"
}

func appInfo() (g string) {
	return appName + " " + appVersion
}

func defaultHandler(c *gin.Context) {
	c.String(200, appInfo())
}

func greetHandler(c *gin.Context) {
	name := c.Param("name")
	c.String(200, greeting(name))
}

func getRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/", defaultHandler)
	router.GET("/greet/:name", greetHandler)
	return router
}

func main() {
	err := getRouter().Run()
	if err != nil {
		panic("Error encountered while serving: " + err.Error())
	}
}
