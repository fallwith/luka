// main_test.go

// the package name should match that defining the functionality being tested
package main

import (
	// ioutil is used here to read response bodies
	"io/ioutil"
	// http and httptest are used here for HTTP testing
	"net/http"
	"net/http/httptest"
	// testing is the built-in Go testing package
	"testing"
)

// This is a helper function that all route tests can leverage.
// If this helper would be of use to other files containing tests, it would
// typically be moved out into a dedicated helpers file.
//
// receives:
//   t            - the Go unit test object
//   method       - the HTTP method (GET, POST, etc.) to use
//   uri          - the URI to test
//   expectedBody -
//
// returns:
//   (none) - the function directly use the `t` object to generate test errors
func sendTestRequest(t *testing.T, method string, uri string, expectedBody string) {
	// With `main.go`'s `getRouter()` exposing the Gin router object, the
	// test can get at it
	r := getRouter()
	// Use Go's built-in HTTP testing functionality to define a request and
	// then use the router instance to service it
	rec := httptest.NewRecorder()
	req, _ := http.NewRequest(method, uri, nil)
	r.ServeHTTP(rec, req)
	// Expect the call to return with an "OK" HTTP code
	if rec.Code != http.StatusOK {
		t.Errorf("Expected a success from the '" + uri + "' uri, but received an error")
	}
	// Parse the response body and make sure it matches what the helper function
	// was told it should be
	body, _ := ioutil.ReadAll(rec.Result().Body)
	if string(body) != expectedBody {
		t.Errorf("Expected the response body to match '" + expectedBody +
			"', but got '" + string(body) + "'")
	}
}

// Test the default (/) route
func TestDefaultRoute(t *testing.T) {
	// With `appInfo()` dedicated to yielding a string with the app name and
	// version, it can be invoked by the testing layer
	expectedBody := appInfo()
	// Call the helper function with default route specific values to do the
	// actual testing
	sendTestRequest(t, "GET", "/", expectedBody)
}

// Test the greet (/greet/:name) route
func TestGreetRoute(t *testing.T) {
	// Place a name in a variable and use that variable multiple times instead
	// of repeating the name string. This cuts down on programmer error and helps
	// with maintainability.
	name := "Indiana"
	// With the `greeting()` function yielding a greeting for a given name,
	// we can determine what the greeting should be prior to hitting the route
	expectedBody := greeting(name)
	// Call the helper function with the greeting route specific values to do
	// the actual testing
	sendTestRequest(t, "GET", "/greet/"+name, expectedBody)
}
