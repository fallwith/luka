# Dockerfile
FROM ubuntu:18.04

WORKDIR /app
COPY luka /app

EXPOSE 8080

CMD ["/app/luka"]
