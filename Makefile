# Makefile
build: deps
	go build -o luka main.go
build-linux: deps
	env GOOS=linux go build -o luka main.go
clean:
	rm -f luka
curl:
	curl http://localhost:8080
	curl http://localhost:8080/greet/Luka
deps:
	go get github.com/gin-gonic/gin
	go get github.com/alecthomas/gometalinter
	gometalinter --install
docker-build: build-linux
	docker build -t luka .
docker-clean: clean
	docker rmi luka
docker-run: docker-build
	docker run --rm -itp 8080:8080 luka
lint:
	gometalinter --deadline 5m ./...
run: deps
	go run main.go
test: deps
	go test -v ./...
